const express = require("express");
const router = express.Router();
const apiRoutes = require("./api");
const debug = require("debug")("irun:routes");

router.get("/", (req, res) => {
  debug(req.method + " " + req.url);
  let options = {
    root: `${__dirname}/../html`
  };
  res.sendFile("index.html", options);
});

router.use("/api", apiRoutes);

module.exports = router;
