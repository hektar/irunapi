const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../../auth");
const Tracks = mongoose.model("Tracks");
const Activities = mongoose.model("Activities");
const Users = mongoose.model("Users");
const debug = require("debug")("irun:api/tracks");
const fetch = require("node-fetch");
const _ = require("lodash");
const moment = require("moment");
const elevationAPI_IP = "http://172.18.0.4:8080/"; // externer Zugriff bspw local "http://167.99.247.144:8080/"; interner Zugriff von docker //http://172.18.0.4:8080/
moment.locale("de");

//POST neuen Track anlegen
router.post("/", auth.required, async (req, res, next) => {
  try {
    debug(req.method + " " + req.url + "  %O", req.body);
    let userId = req.payload.id;
    let rawTrack = req.body;

    if (rawTrack.coords.length < 2) {
      return res.status(400).json({
        error: {
          msg: "Keine Streckendaten vorhanden"
        }
      });
    }

    let processedTrack = await processTrack(rawTrack);

    debug(processedTrack.totalDistance);
    if (processedTrack.totalDistance < 100) {
      return res.status(400).json({
        error: {
          msg: "Gelaufene Strecke sollte mindestens 100 Meter lang sein"
        }
      });
    }

    // Erzeuge die Strecke
    let track = await new Tracks(processedTrack).save();

    let checkpointTimes = track.trackProfile.map((cp, idx, cps) => {
      if (idx == 0) {
        title = "Start";
      } else if (idx == cps.length - 1) {
        title = "Ziel";
      } else {
        title = `Checkpoint ${cp.distanceFromStart}m`;
      }
      return {
        checkpointId: cp._id,
        title: title,
        distance: cp.distanceFromStart,
        time: cp.time
      };
    });

    let rawActivity = {
      title: moment().format("DD.MM.YYYY HH:mm"),
      date: moment(),
      user: userId,
      track: track._id,
      checkpointTimes: checkpointTimes,
      finishTime: checkpointTimes[checkpointTimes.length - 1].time
    };

    // Erzeuge die Aktivität
    let activity = await new Activities(rawActivity).save();

    // TODO Auslagern in Funktion
    // Ermittle Erfahrungspunkte.
    let xp = 0;
    if (track.totalDistance >= 1000) {
      // 10 EXP pro KM
      xp = Math.floor(track.totalDistance / 100) * 10;
    }

    // Aktualisiere die EXP vom User
    await Users.findByIdAndUpdate(userId, { $inc: { exp: xp } });

    // Aktualisiere die kürzlich hinzugefügte Strecke
    Tracks.findOneAndUpdate(
      { _id: track._id },
      { $addToSet: { activities: activity._id } },
      function(err, updatedTrack) {
        debug("updatedTrack", updatedTrack);
        if (err) {
          debug(err);
          return res.status(500).end();
        }
        return res.status(200).end();
      }
    );
  } catch (error) {
    next(error);
  }
});

//GET Liefert alle Tracks vom User
router.get("/", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %s", req.payload.id);
  let userId = req.payload.id;
  let bestTime = req.query.bestTime === "true" || req.query.bestTime == 1;

  // Liefere alle Strecken mit der besten Zeit
  if (bestTime) {
    Tracks.find({ user: userId })
      .populate({
        path: "activities",
        select: "checkpointTimes"
      })
      .lean()
      .exec(function(err, tracks) {
        if (err) {
          debug(err);
          return res.sendStatus(500);
        }
        // Ermittle die Bestzeit von den abgeschlossenen Aktivitäten
        tracks.forEach(track => {
          let bestTime = null;
          track.activities.forEach(activity => {
            let lastCP =
              activity.checkpointTimes[activity.checkpointTimes.length - 1];
            if (bestTime) {
              bestTime = bestTime <= lastCP.time ? bestTime : lastCP.time;
            } else {
              bestTime = lastCP.time;
            }
          });
          track.bestTime = bestTime;
        });

        return res.status(200).send(tracks);
      });
  } else {
    Tracks.find({ user: userId }).exec(function(err, tracks) {
      if (err) return res.sendStatus(500);
      return res.status(200).send(tracks);
    });
  }
});

// Vergleicht zwei Strecken miteinander und liefert ihre Ähnlichkeit zurück
router.get("/compare", auth.required, async (req, res, next) => {
  debug(req.method + " " + req.url + "  %o", req.query);
  let trackId1 = req.query.t1;
  let trackId2 = req.query.t2;
  let compareTotalDistance = req.query.total === "true" || req.query.total == 1;
  let track1 = await Tracks.findById(trackId1).populate({
    path: "activities",
    // select: "-track",
    options: { sort: { date: -1 } }
  });
  let track2 = await Tracks.findById(trackId2).populate({
    path: "activities",
    // select: "-track",
    options: { sort: { date: -1 } }
  });
  debug("T1 %j", track1.activities[0].checkpointTimes);
  debug("T2 %j", track2.activities[0].checkpointTimes);
  let result = await compareTracks(
    track1,
    track2,
    compareTotalDistance,
    !compareTotalDistance
  );
  // debug(result);
  return res.status(200).send({
    similarity: result.similarity,
    track1: result.track1,
    track2: result.track2
  });
});

// Liefert die Strecken von allen Freunden
router.get("/friends", auth.required, async (req, res, next) => {
  let userId = req.payload.id;
  Users.findById(userId, "-salt -hash").exec(function(errUsr, user) {
    if (errUsr) return res.sendStatus(500);
    Tracks.find({ user: { $in: user.friendList } }, "-activities")
      .populate("user", "username")
      .exec(function(errTrck, tracksOfFriends) {
        if (errTrck) return res.sendStatus(500);
        return res.status(200).send(tracksOfFriends);
      });
  });
});

//GET Liefert alle bereits absolvierten Strecken
router.get("/completed", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %s", req.payload.id);
  let userId = req.payload.id;
  Tracks.find({ user: userId, activities: { $exists: true, $ne: [] } })
    .populate({
      path: "activities",
      select: "-track",
      options: { sort: { date: -1 } }
    }) // Absteigend
    .exec(function(err, tracks) {
      if (err) return res.sendStatus(500);
      return res.status(200).send(tracks);
    });
});

// TESTROUTE GET Routen zu Testzwecken
router.post("/test", auth.optional, async (req, res, next) => {
  let body = req.body;
  let path = body.locations;

  let promises = [];
  // Zerteile die Koordinaten in Pakete für die Elevation-API
  let chunk = 1000;
  let passing = Math.ceil(path.length / chunk) * chunk;
  for (let index = 0; index < passing; index += chunk) {
    let package = _.slice(path, index, index + chunk);
    let body = {
      locations: package
    };
    // Schicke ein Request an die Elevation API um die Höheninformationen zu erhalten
    promises.push(
      fetch(`${elevationAPI_IP}api/v1/lookup`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => {
          debug(res.status);
          return res.json();
        })
        .then(json => {
          return json;
        })
        .catch(err => {
          debug(err);
        })
    );
  }
  let elevations = await Promise.all(promises);

  debug(result);

  return res.end();
});

// TESTROUTE GET Erzeugt ein Punkt verschoben um x Meter in gewünschter Richtung
router.get("/offset", auth.optional, (req, res, next) => {
  let lat = req.query.lat;
  let lng = req.query.lng;

  return res.status(200).send(offsetPoint(lat, lng, 90, 100));
});

// TESTROUTE GET Schneidet die Strecke
router.get("/cut", auth.optional, (req, res, next) => {
  try {
    debug(req.method + " " + req.url + "  %O", req.query);
    let trackId = req.query.id;
    let distance = req.query.distance;
    Tracks.findById(trackId).exec(async function(err, track) {
      if (err) return res.sendStatus(500);
      if (track) {
        let cuttedTrack = await cutTrack(track, distance);
        return res.status(200).send(cuttedTrack);
      } else {
        return res.status(404).end();
      }
    });
  } catch (error) {
    next(error);
  }
});

// TESTROUTE GET Ermittelt die Kompassrichtung
router.get("/bearing", auth.optional, (req, res, next) => {
  let lat1 = req.query.lat1;
  let lng1 = req.query.lng1;
  let lat2 = req.query.lat2;
  let lng2 = req.query.lng2;
  debug(bearing(lat1, lng1, lat2, lng2));
  return res.status(200).json({ bearing: bearing(lat1, lng1, lat2, lng2) });
});

// -----------------

function ratio(x, y) {
  if (x >= 0 && x <= y && y != 0) {
    return x / y;
  } else if (x > y) {
    return y / x;
  } else if (x == 0 && y == 0) {
    return 1;
  }
}

function ratioDistance(x, y) {
  if (x <= y) {
    return x / y;
  } else {
    return y / x;
  }
}

function toRad(degree) {
  return (degree * Math.PI) / 180;
}

function toDeg(radiant) {
  return (radiant * 180) / Math.PI;
}

// https://www.movable-type.co.uk/scripts/latlong.html
function bearing(lat1, lng1, lat2, lng2) {
  //Differenz Längengrad
  let dLng = toRad(lng2 - lng1);

  //Umwandeln in Radiant
  deltaLat1 = toRad(lat1);
  deltaLat2 = toRad(lat2);
  deltaLng1 = toRad(lng1);

  let y = Math.sin(dLng) * Math.cos(deltaLat2);
  let x =
    Math.cos(deltaLat1) * Math.sin(deltaLat2) -
    Math.sin(deltaLat1) * Math.cos(deltaLat2) * Math.cos(dLng);
  let brng = toDeg(Math.atan2(y, x));
  return (brng + 360) % 360;
}

//Destination point given distance and bearing from start point
//Startpunkt(lat, long), Richtung(0-360°), Entfernung in m
function offsetPoint(pLat1, pLng1, brng, dist) {
  const R = 6371000; // Erdradius ca. 6371km auf unseren Breitengraden
  let angDist = dist / R;

  //Differenz Längengrad

  //Umwandeln in Radiant
  deltaLat1 = toRad(pLat1);
  deltaLng1 = toRad(pLng1);
  deltaBrng = toRad(brng);

  let latDest = Math.asin(
    Math.sin(deltaLat1) * Math.cos(angDist) +
      Math.cos(deltaLat1) * Math.sin(angDist) * Math.cos(deltaBrng)
  );
  let lngDest =
    deltaLng1 +
    Math.atan2(
      Math.sin(deltaBrng) * Math.sin(angDist) * Math.cos(deltaLat1),
      Math.cos(angDist) - Math.sin(deltaLat1) * Math.sin(latDest)
    );
  let normalizedLng = ((toDeg(lngDest) + 540) % 360) - 180;

  // return `${toDeg(latDest)},${normalizedLng}`;
  return { latitude: toDeg(latDest), longitude: normalizedLng };
}

/**
 * Ermittelt den Mittelpunkt zwischen zwei Koordinaten.
 *
 * Quelle: http://jsfiddle.net/kevinrignault/gzq64p56/
 *
 * @param {number} lat1 Breitengrad Punkt 1
 * @param {number} lng1 Längengrad Punkt 1
 * @param {number} lat2 Breitengrad Punkt 2
 * @param {number} lng2 Längengrad Punkt 2
 *
 * @returns {Object} Mittelpunktkoordinate
 */
function middlePoint(lat1, lng1, lat2, lng2) {
  //Differenz Längengrad
  let dLng = toRad(lng2 - lng1);

  //Umwandeln in Radiant
  deltaLat1 = toRad(lat1);
  deltaLat2 = toRad(lat2);
  deltaLng1 = toRad(lng1);

  let bX = Math.cos(deltaLat2) * Math.cos(dLng);
  let bY = Math.cos(deltaLat2) * Math.sin(dLng);
  let lat3 = Math.atan2(
    Math.sin(deltaLat1) + Math.sin(deltaLat2),
    Math.sqrt((Math.cos(deltaLat1) + bX) * (Math.cos(deltaLat1) + bX) + bY * bY)
  );
  let lng3 = deltaLng1 + Math.atan2(bY, Math.cos(deltaLat1) + bX);

  return {
    latitude: toDeg(lat3),
    longitude: toDeg(lng3)
  };
}

/**
 * Ermittelt die Entfernung (Luftlinie) zwischen zwei Koordinaten
 * nach der Haversine Formel in Meter.
 *
 * Quelle: https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
 *
 * @param {number} lat1 Breitengrad Punkt1
 * @param {number} lon1 Längengrad Punkt1
 * @param {number} lat2 Breitengrad Punkt2
 * @param {number} lon2 Längengrad Punkt2
 *
 * @returns {number} Entfernung in meter
 */
function haversineDistance(lat1, lon1, lat2, lon2) {
  let p = Math.PI / 180; // Math.PI / 180
  let a =
    0.5 -
    Math.cos((lat2 - lat1) * p) / 2 +
    (Math.cos(lat1 * p) *
      Math.cos(lat2 * p) *
      (1 - Math.cos((lon2 - lon1) * p))) /
      2;
  let distance = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  return distance * 1000;
}

/**
 * Schneidet die Strecke auf die gewünschte Länge zu
 * @param {*} track Die Strecke die geschnitten wird
 * @param {*} cutDistance Die gewünschte Schnittlänge
 * @param {*} forRanking Ermittle die Zielzeit für die Rangliste oder CheckpointTimes für Einzelvergleich
 */
async function cutTrack(track, cutDistance, meters = 100, forRanking = false) {
  function DistanceException(message) {
    this.message = message;
    this.name = "DistanceException";
  }
  debug("cutTrack", forRanking);
  if (cutDistance > track.totalDistance) {
    throw new DistanceException(
      "Die Schnittlänge ist größer als die Strecke selbst"
    );
  } else {
    let finalCoordAdded = false;
    let segmentedCoords = [];
    let totalDistance = 0;
    let segmentFactor = 1;
    let segmentValue = meters;
    let segmentLength = segmentFactor * segmentValue;

    for (let i = 0; i < track.coords.length; i++) {
      //Abbruch der Schleife, wenn die Schnittlänge erreicht wurde
      if (finalCoordAdded) {
        break;
      }
      // Abbruch der Schleife beim letzten Element
      if (track.coords.length - 1 == i) {
        // Prüfe, ob im Array bereits der Punkt hinzugefügt wurde
        let index = segmentedCoords.findIndex(
          p =>
            p.latitude === track.coords[i].latitude &&
            p.longitude === track.coords[i].longitude
        );
        if (index === -1) {
          segmentedCoords.push(coords[i]);
        }
        break;
      }

      let p1 = track.coords[i];
      let p2 = track.coords[i + 1];

      // Prüfe, ob im Array bereits der Punkt hinzugefügt wurde
      let index = segmentedCoords.findIndex(
        p => p.latitude === p1.latitude && p.longitude === p1.longitude
      );
      if (index === -1) {
        segmentedCoords.push(p1);
      }

      if (i == 0) {
        p1.distanceToPrevPoint = 0;
        p1.distanceFromStart = totalDistance;
      }

      let distance = haversineDistance(
        p1.latitude,
        p1.longitude,
        p2.latitude,
        p2.longitude
      );

      totalDistance += distance;

      p2.distanceToPrevPoint = distance;
      p2.distanceFromStart = totalDistance;

      if (totalDistance >= cutDistance) {
        let valToAdd = cutDistance - p1.distanceFromStart;
        let brng = bearing(
          p1.latitude,
          p1.longitude,
          p2.latitude,
          p2.longitude
        );
        let finalCoord = offsetPoint(p1.latitude, p1.longitude, brng, valToAdd);
        finalCoord.isCheckpoint = false;
        finalCoord.distanceToPrevPoint = valToAdd;
        finalCoord.distanceFromStart = cutDistance;

        p2 = finalCoord;
        finalCoordAdded = true;
      }

      if (p2.distanceFromStart <= segmentLength) {
        if (p2.distanceFromStart == segmentLength) {
          p2.isCheckpoint = true;
          segmentFactor++;
          segmentLength = segmentFactor * segmentValue;
        }
        segmentedCoords.push(p2);
        continue;
      } else {
        do {
          let valToAdd = segmentLength - p1.distanceFromStart;
          let brng = bearing(
            p1.latitude,
            p1.longitude,
            p2.latitude,
            p2.longitude
          );
          let newCoord = offsetPoint(p1.latitude, p1.longitude, brng, valToAdd);

          newCoord.isCheckpoint = true;
          newCoord.distanceToPrevPoint = valToAdd;
          newCoord.distanceFromStart = segmentLength;

          segmentedCoords.push(newCoord);

          distance = distance - valToAdd;

          p2.distanceToPrevPoint = distance;

          segmentFactor++;
          segmentLength = segmentFactor * segmentValue;

          p1 = newCoord;
        } while (p2.distanceToPrevPoint > segmentValue);
      }
    }

    // Ermittle die Höhe
    let trackData = await determineElevation(segmentedCoords);
    // Ermittle Streckeninformationen wie max, min Höhe etc.
    let trackInfo = determinTrackInformation(trackData.coordsWithElevation);
    // Ermittle Zielzeit für die Rangliste oder CheckpointTimes für Einzelvergleich.
    let finishTime = track.activities[0].finishTime;
    let cuttedCheckpointTimes = [];
    if (forRanking) {
      for (
        let index = 0;
        index < track.activities[0].checkpointTimes.length;
        index++
      ) {
        let cp = track.activities[0].checkpointTimes[index];
        if (cp.distance >= trackInfo.totalDistance) {
          finishTime = interpolateTime(
            trackInfo.totalDistance,
            track.activities[0].checkpointTimes[index - 1].distance,
            cp.distance,
            track.activities[0].checkpointTimes[index - 1].time,
            cp.time
          );
          cuttedCheckpointTimes.push({
            distance: trackInfo.totalDistance,
            time: finishTime,
            title: "Ziel"
          });
          break;
        }
        cuttedCheckpointTimes.push(cp);
      }
    }

    let finalCuttedTrack = {
      _id: track._id,
      finishTime: finishTime,
      title: track.title,
      cuttedTimes: cuttedCheckpointTimes,
      activities: track.activities,
      user: track.user,
      date: moment(),
      coords: trackData.coordsWithElevation,
      trackProfile: trackData.trackProfile,
      totalDistance: trackInfo.totalDistance,
      ascent: trackInfo.ascent,
      descent: trackInfo.descent,
      minElevation: trackInfo.minElevation,
      maxElevation: trackInfo.maxElevation,
      elevationDifference: trackInfo.elevationDiff
    };

    return finalCuttedTrack;
  }
}

/**
 * Vergleicht zwei Strecken nach der vorgestellten Methode in der BA
 *
 * @param {*} track1 Strecke 1
 * @param {*} track2 Strecke 2
 * @param {*} compareTotalDistance Gesamtstreckenlänge soll berücksichtigt werden
 * @param {*} forRanking Streckenvergleich für Rangliste
 */
function compareTracks(
  track1,
  track2,
  compareTotalDistance = true,
  forRanking = false
) {
  return new Promise(async (resolve, reject) => {
    let similarity = 0;

    //Prüfe auf Fall 1... alle Werte müssen >=0 sein.
    if (
      track1.ascent >= 0 &&
      track1.descent >= 0 &&
      track1.minElevation >= 0 &&
      track1.maxElevation >= 0 &&
      track1.elevationDifference >= 0 &&
      track2.ascent >= 0 &&
      track2.descent >= 0 &&
      track2.minElevation >= 0 &&
      track2.maxElevation >= 0 &&
      track2.elevationDifference >= 0
    ) {
      // Nach Gleichnung (7) in der Bachelorarbeit
      if (compareTotalDistance) {
        similarity =
          (ratioDistance(track1.totalDistance, track2.totalDistance) +
            ratio(track1.minElevation, track2.minElevation) +
            ratio(track1.maxElevation, track2.maxElevation) +
            ratio(track1.elevationDifference, track2.elevationDifference) +
            ratio(track1.ascent, track2.ascent) +
            ratio(track1.descent, track2.descent)) /
          6;
      } else {
        let smallerTrackLength = Math.min(
          track1.totalDistance,
          track2.totalDistance
        );

        if (track1.totalDistance > track2.totalDistance) {
          track1 = await cutTrack(
            track1,
            smallerTrackLength,
            undefined,
            forRanking
          );
          // <=(kleinergleich), weil sonst finishTime in der Rangliste bei nachgelaufenen Strecken von Freunden nicht gefüllt ist
        } else if (track1.totalDistance <= track2.totalDistance) {
          track2 = await cutTrack(
            track2,
            smallerTrackLength,
            undefined,
            forRanking
          );
        }

        similarity =
          (1 +
            ratio(track1.minElevation, track2.minElevation) +
            ratio(track1.maxElevation, track2.maxElevation) +
            ratio(track1.elevationDifference, track2.elevationDifference) +
            ratio(track1.ascent, track2.ascent) +
            ratio(track1.descent, track2.descent)) /
          6;
        debug("similarity", similarity);
      }
    } else {
      // Fall 2 od. 3
      // Nach Gleichnung (6) in der Bachelorarbeit
      similarity =
        (ratio(track1.elevationDifference, track2.elevationDifference) +
          ratio(track1.ascent, track2.ascent) +
          ratio(track1.descent, track2.descent)) /
        5;
    }

    if (isNaN(similarity)) {
      reject(new Error("Konnte Strecken nicht vergleichen"));
    }

    resolve({
      similarity: parseFloat(similarity.toFixed(2)),
      track1: track1,
      track2: track2
    });
  });
}

// Verarbeitet die gesamte Strecke für die DB
async function processTrack(track) {
  // Segmentiere die Strecke
  let segmentedCoords = segmentTrack(track.coords);
  // Ermittle die Höhe
  let trackData = await determineElevation(segmentedCoords);
  // Ermittle Streckeninformationen wie max, min Höhe etc.
  let trackInfo = determinTrackInformation(trackData.coordsWithElevation);

  let processedTrack = {
    title: track.title,
    user: track.userId,
    date: moment(),
    coords: trackData.coordsWithElevation,
    trackProfile: trackData.trackProfile,
    totalDistance: trackInfo.totalDistance,
    ascent: trackInfo.ascent,
    descent: trackInfo.descent,
    minElevation: trackInfo.minElevation,
    maxElevation: trackInfo.maxElevation,
    elevationDifference: trackInfo.elevationDiff
  };
  return processedTrack;
}

/**
 * Lineare Interpolation der Zeit gemäß der Gleichung (siehe Bachlorarbeit).
 * Das Ergebnis wird gerundet.
 * @param {*} x
 * @param {*} x1
 * @param {*} x2
 * @param {*} fx1
 * @param {*} fx2
 */
function interpolateTime(x, x1, x2, fx1, fx2) {
  return Math.round(fx1 + ((x - x1) / (x2 - x1)) * (fx2 - fx1));
}

/**
 * Segmentiert die Strecke in N Meter und liefert
 * die Koordinaten in einem Array zurück.
 *
 * @param {Object[]} coords Die Streckenkoordinatetn
 * @param {number} meters Segmentierungsabstand
 */
function segmentTrack(coords, meters = 100) {
  let segmentedCoords = [];
  let totalDistance = 0;
  let segmentFactor = 1;
  let segmentValue = meters;
  let segmentLength = segmentFactor * segmentValue;

  for (let i = 0; i < coords.length; i++) {
    // Abbruch der Schleife beim letzten Element
    if (coords.length - 1 == i) {
      // Prüfe, ob im Array bereits der Punkt hinzugefügt wurde
      let index = segmentedCoords.findIndex(
        p =>
          p.latitude === coords[i].latitude &&
          p.longitude === coords[i].longitude
      );
      if (index === -1) {
        segmentedCoords.push(coords[i]);
      }
      break;
    }

    let p1 = coords[i];
    let p2 = coords[i + 1];

    // Prüfe, ob im Array bereits der Punkt hinzugefügt wurde
    let index = segmentedCoords.findIndex(
      p => p.latitude === p1.latitude && p.longitude === p1.longitude
    );
    if (index === -1) {
      segmentedCoords.push(p1);
    }

    if (i == 0) {
      p1.distanceToPrevPoint = 0;
      p1.distanceFromStart = totalDistance;
      p1.sortsequence = i;
    }

    let distance = haversineDistance(
      p1.latitude,
      p1.longitude,
      p2.latitude,
      p2.longitude
    );

    totalDistance += distance;

    p2.distanceToPrevPoint = distance;
    p2.distanceFromStart = totalDistance;
    p2.sortsequence = i + 1;

    if (totalDistance <= segmentLength) {
      if (totalDistance == segmentLength) {
        p2.isCheckpoint = true;
        segmentFactor++;
        segmentLength = segmentFactor * segmentValue;
      }
      segmentedCoords.push(p2);
      continue;
    } else {
      do {
        let valToAdd = segmentLength - p1.distanceFromStart;
        let brng = bearing(
          p1.latitude,
          p1.longitude,
          p2.latitude,
          p2.longitude
        );
        let newCoord = offsetPoint(p1.latitude, p1.longitude, brng, valToAdd);

        newCoord.isCheckpoint = true;
        newCoord.distanceToPrevPoint = valToAdd;
        newCoord.distanceFromStart = segmentLength;
        newCoord.time = interpolateTime(
          segmentLength,
          p1.distanceFromStart,
          p2.distanceFromStart,
          p1.time,
          p2.time
        );

        segmentedCoords.push(newCoord);

        distance = distance - valToAdd;

        p2.distanceToPrevPoint = distance;

        segmentFactor++;
        segmentLength = segmentFactor * segmentValue;

        p1 = newCoord;
      } while (p2.distanceToPrevPoint > segmentValue);
    }
  }
  return segmentedCoords;
}

function determinTrackInformation(coords) {
  let maxElevation = -1;
  let minElevation = -1;
  let ascent = 0;
  let descent = 0;
  let prevEle = -1;
  let totalDistance = coords[coords.length - 1].distanceFromStart;

  coords.forEach(coord => {
    // Ermittle Gesammtauf- und abstieg
    if (prevEle != -1) {
      let diff = coord.elevation - prevEle;
      if (diff >= 0) {
        ascent += diff;
      } else {
        descent += diff;
      }
    }

    // Initialisiere min und max
    if (minElevation == -1) {
      minElevation = coord.elevation;
    }
    if (maxElevation == -1) {
      maxElevation = coord.elevation;
    }
    // Ermittle min und max Höhe
    if (minElevation > coord.elevation) {
      minElevation = coord.elevation;
    }
    if (maxElevation < coord.elevation) {
      maxElevation = coord.elevation;
    }

    prevEle = coord.elevation;
  });

  return {
    ascent: ascent,
    descent: Math.abs(descent),
    elevationDiff: maxElevation - minElevation,
    minElevation: minElevation,
    maxElevation: maxElevation,
    totalDistance: totalDistance
  };
}

// Google Elevation API benötigt Kreditkarteninformationen daher wird Open Elevation API als Alternative genutzt
async function determineElevation(segmentedCoords, meters = 100) {
  // Fülle die path variable mit dem long und lat Informationen
  let path = [];
  let promises = [];

  segmentedCoords.forEach(coord => {
    path.push({ latitude: coord.latitude, longitude: coord.longitude });
  });

  // Zerteile die Koordinaten in Pakete für die Elevation-API
  let chunk = 500;
  let passing = Math.ceil(path.length / chunk) * chunk;
  for (let index = 0; index < passing; index += chunk) {
    let package = _.slice(path, index, index + chunk);
    let body = {
      locations: package
    };
    // Schicke ein Request an die Elevation API um die Höheninformationen zu erhalten
    promises.push(
      fetch(`${elevationAPI_IP}api/v1/lookup`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => {
          debug(res.status);
          return res.json();
        })
        .then(json => {
          return json;
        })
        .catch(err => {
          debug(err);
        })
    );
  }

  let rawElevations = await Promise.all(promises);

  let result = [];
  rawElevations.forEach(ele => {
    result = _.concat(result, ele.results);
  });
  let mappedCoords = segmentedCoords.map((coord, idx) => {
    if (
      coord.latitude == result[idx].latitude &&
      coord.longitude == result[idx].longitude
    ) {
      coord.elevation = result[idx].elevation;
      return coord;
    }
  });
  // Filter die Wegpunkte raus ink. Start und Ziel
  let trackProfile = _.filter(mappedCoords, (coord, idx, arr) => {
    return (
      coord.distanceFromStart % meters == 0 || idx == 0 || idx == arr.length - 1
    );
  });

  return {
    coordsWithElevation: mappedCoords,
    trackProfile: trackProfile
  };
}

// Ermittelt den Mittelpunkt zweier Koordinatenpunkte und fügt sie im Array dazwischen
function smoothTrackedData(trackData, repeat = 1) {
  for (let x = 1; x <= repeat; x++) {
    let runs = 1;
    const initalLength = trackData.length - 1;
    let n = 0;
    for (let i = 0; runs <= initalLength; n = 2 * i) {
      // Füge immer zwischen zwei Koordinaten im Array die Mittelpunktkoordinate
      // [a,b] -> [a,m,b]
      trackData.splice(
        n + 1,
        0,
        middlePoint(
          trackData[n].latitude,
          trackData[n].longitude,
          trackData[n + 1].latitude,
          trackData[n + 1].longitude
        )
      );
      i++;
      runs++;
    }
  }
  return trackData;
}

module.exports = { router, compareTracks };
