const mongoose = require("mongoose");
const passport = require("passport");
const router = require("express").Router();
const auth = require("../../auth");
const Users = mongoose.model("Users");
const Activities = mongoose.model("Activities");
const debug = require("debug")("irun:api/users");
const _ = require("lodash");

//POST neuen User anlegen (optional, jeder hat Zugriff)
router.post("/", auth.optional, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);

  const body = req.body;

  if (!body.username) {
    return res.status(422).json({
      errors: {
        username: "wird benötigt"
      }
    });
  }

  if (!body.password) {
    return res.status(422).json({});
  }

  //Prüfe, ob User bereits existiert
  Users.findOne({ username: body.username }, function (err, userObj) {
    if (err) throw err;

    //User existiert, stoppe Registrierung
    if (userObj) {
      debug("%o", userObj);
      return res.status(422).json({
        errors: {
          message: "Benutzer existiert bereits"
        }
      });
    }

    let newUser = new Users(body);
    newUser.setPassword(body.password);

    return newUser.save().then(() => res.json(newUser.toAuthJSON()));
  });
});

//POST Login Route (optional, jeder hat Zugriff)
router.post("/login", auth.optional, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);

  const { user } = req.body;

  if (!user.username) {
    return res.status(422).json({
      errors: {
        username: "wird benötigt"
      }
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "wird benötigt"
      }
    });
  }

  passport.authenticate(
    "local",
    { session: false },
    (err, passportUser, info) => {
      if (err) {
        return next(err);
      }

      if (passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();

        return res.json({ user: user.toAuthJSON() });
      }

      return res.status(400).send(info);
    }
  )(req, res, next);
});

//POST Route zum Hinzufügen eines Freundes (nur für authentifizierte Benutzer)
router.post("/addFriend", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);
  let userID = req.payload.id;
  let usernameOfFriend = req.body.username;

  Users.findOneAndUpdate(
    { username: usernameOfFriend },
    { $addToSet: { friendRequests: userID } },
    { new: true },
    function (err, updatedUser) {
      if (err) {
        debug(err);
        return res.status(500).end();
      }
      debug(updatedUser);
      if (updatedUser) {
        Users.findByIdAndUpdate(
          userID,
          { $addToSet: { sentFriendRequests: updatedUser._id } },
          { new: true },
          function (err, updatedUser) {
            if (err) {
              debug(err);
              return res.status(500).end();
            }
            debug(updatedUser);
            return res.status(200).end();
          }
        );
      } else {
        res.status(404).end();
      }
    }
  );
});

//POST Route zum Akzeptieren einer Freundschaftsanfrage (nur für authentifizierte Benutzer)
router.post("/acceptFriend", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);
  let userID = req.payload.id;
  let idOfFriend = req.body.id;

  Users.findByIdAndUpdate(
    idOfFriend,
    {
      $pull: { sentFriendRequests: userID },
      $addToSet: { friendList: userID }
    },
    { new: true },
    function (err, updatedUser) {
      if (err) {
        debug(err);
        return res.status(500).end();
      }
      debug(updatedUser);
      Users.findByIdAndUpdate(
        userID,
        {
          $pull: { friendRequests: updatedUser._id },
          $addToSet: { friendList: updatedUser._id }
        },
        { new: true },
        function (err, updatedUser) {
          if (err) {
            debug(err);
            return res.status(500).end();
          }
          return res.status(200).end();
        }
      );
    }
  );
});

//POST Route zum Akzeptieren einer Freundschaftsanfrage (nur für authentifizierte Benutzer)
router.post("/declineFriend", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);

  let userID = req.payload.id;
  let idOfFriend = req.body.id;

  Users.findByIdAndUpdate(
    idOfFriend,
    {
      $pull: { sentFriendRequests: userID }
    },
    { new: true },
    function (err, updatedUser) {
      if (err) {
        debug(err);
        return res.status(500).end();
      }
      debug(updatedUser);
      Users.findByIdAndUpdate(
        userID,
        {
          $pull: { friendRequests: updatedUser._id }
        },
        { new: true },
        function (err, updatedUser) {
          if (err) {
            debug(err);
            return res.status(500).end();
          }
          return res.status(200).end();
        }
      );
    }
  );
});

//GET Freundesliste abrufen (required, nur für authentifizierte Benutzer)
router.get("/friends", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);
  const { id } = req.payload;
  Users.findById(id, "-salt -hash")
    .populate("friendRequests", "_id username")
    .populate("sentFriendRequests", "_id username")
    .populate("friendList", "_id username")
    .exec(function (err, user) {
      if (err) return res.sendStatus(500);

      debug("USER", user);

      return res.status(200).send(user);
    });
});

//GET Info zum Freund abrufen (required, nur für authentifizierte Benutzer)
router.get("/friends/:id/info", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.body);
  const userId = req.payload.id;
  const friendId = req.params.id;

  Users.findOne(
    { _id: friendId, friendList: { $in: [userId] } },
    "_id, username"
  )
    .lean() // -> liefert das JSON-Objekt anstatt das Model-Objekt
    .exec(function (err, friend) {
      if (err) return res.sendStatus(500);
      Activities.find({ user: friend._id }, "-user")
        .populate("track", "-activities")
        .exec(function (err, activities) {
          if (err) return res.sendStatus(500);
          friend["activities"] = activities;
          return res.status(200).send({
            friend: friend
          });
        });
    });
});

//GET Aktuellen User abfragen (required, nur für authentifizierte Benutzer)
router.get("/current", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %O", req.payload);
  const { id } = req.payload;

  Users.findById(id).then(user => {
    if (!user) {
      res.sendStatus(400);
    }

    res.json({ user: user.toAuthJSON() });
  });
});

//GET User abfragen (required, nur für authentifizierte Benutzer)
router.get("/:id", auth.required, (req, res, next) => {
  debug(req.method + " user with ID: %s", req.params.id);

  Users.findById(req.params.id, "-hash -salt", function (err, user) {
    if (!user) return res.sendStatus(404);

    res.status(200).send(user);
  });
});

module.exports = router;
