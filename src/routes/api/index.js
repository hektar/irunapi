const express = require("express");
const router = express.Router();

router.use("/users", require("./users"));
router.use("/tracks", require("./tracks").router);
router.use("/activities", require("./activities"));
router.use("/rankings", require("./rankings"));

module.exports = router;
