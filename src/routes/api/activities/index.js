const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../../auth");
const Tracks = mongoose.model("Tracks");
const Users = mongoose.model("Users");
const Activities = mongoose.model("Activities");
const debug = require("debug")("irun:api/tracks");
const _ = require("lodash");
const moment = require("moment");
moment.locale("de");

//GET Liefert alle Aktivitäten vom User
router.get("/", auth.required, (req, res, next) => {
  debug(req.method + " " + req.url + "  %s", req.payload.id);
  let userId = req.payload.id;
  Activities.find({ user: userId })
    .populate("track", "-user")
    .populate("user", "_id username")
    .exec(function(err, activity) {
      if (err) {
        return res.sendStatus(500);
      } else {
        return res.status(200).send(activity);
      }
    });
});

//Post Erstellt eine neue Aktivität
router.post("/", auth.required, async (req, res, next) => {
  debug(req.method + " " + req.url + "  %s", req.body);
  let userId = req.payload.id;
  let body = req.body;
  if (body.isFriendTrack) {
    // Die gelaufenen Freundesstrecke
    let friendTrack = await Tracks.findById(body.trackId, "-activities -date")
      .populate("user")
      .lean();

    let newTrack = {
      title: `${friendTrack.user.username}'s ${friendTrack.title} nachgelaufen`,
      user: userId,
      date: moment(),
      coords: friendTrack.coords,
      trackProfile: friendTrack.trackProfile,
      totalDistance: friendTrack.totalDistance,
      ascent: friendTrack.ascent,
      descent: friendTrack.descent,
      minElevation: friendTrack.minElevation,
      maxElevation: friendTrack.maxElevation,
      elevationDifference: friendTrack.elevationDifference,
      originTrack: friendTrack._id
    };

    // Erzeuge neue Strecke aus der gelaufenen Freundesstrecke
    let track = await new Tracks(newTrack).save();

    let rawActivity = {
      title: moment().format("DD.MM.YYYY HH:mm"),
      date: moment(),
      user: userId,
      track: track._id,
      checkpointTimes: body.checkpointTimes,
      finishTime: body.checkpointTimes[body.checkpointTimes.length - 1].time
    };

    // Ermittle Erfahrungspunkte.
    let xp = 0;
    if (track.totalDistance >= 1000) {
      // 10 EXP pro KM + 100 EXP Bonus
      let xpPerKm = Math.floor(track.totalDistance / 1000) * 10;
      xp = 100 + xpPerKm;
    }

    // Aktualisiere die EXP vom User
    await Users.findByIdAndUpdate(userId, { $inc: { exp: xp } });

    let activity = await new Activities(rawActivity).save();

    Tracks.findOneAndUpdate(
      { _id: track._id },
      { $addToSet: { activities: activity._id } },
      function(err, updatedTrack) {
        if (err) {
          debug(err);
          return res.status(500).end();
        }
        debug(updatedTrack);
        return res.status(200).end();
      }
    );
  } else {
    let rawActivity = {
      title: moment().format("DD.MM.YYYY HH:mm"),
      date: moment(),
      user: userId,
      track: body.trackId,
      checkpointTimes: body.checkpointTimes,
      finishTime: body.checkpointTimes[body.checkpointTimes.length - 1].time
    };

    let track = await Tracks.findById(body.trackId, "totalDistance");

    // Ermittle Erfahrungspunkte.
    let xp = 0;
    if (track.totalDistance >= 1000) {
      // 10 EXP pro KM + 100 EXP Bonus
      let xpPerKm = Math.floor(track.totalDistance / 1000) * 10;
      xp = 100 + xpPerKm;
    }

    // Aktualisiere die EXP vom User
    await Users.findByIdAndUpdate(userId, { $inc: { exp: xp } });

    let activity = await new Activities(rawActivity).save();

    Tracks.findOneAndUpdate(
      { _id: body.trackId },
      { $addToSet: { activities: activity._id } },
      function(err, updatedTrack) {
        if (err) {
          debug(err);
          return res.status(500).end();
        }
        debug(updatedTrack);
        return res.status(200).end();
      }
    );
  }
});

module.exports = router;
