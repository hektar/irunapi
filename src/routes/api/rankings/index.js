const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../../auth");
const Tracks = mongoose.model("Tracks");
const Users = mongoose.model("Users");
const debug = require("debug")("irun:api/rankings");
const moment = require("moment");
const compareTracks = require("../tracks").compareTracks;
moment.locale("de");

router.get("/", auth.required, async (req, res, next) => {
  try {
    debug(req.method + " " + req.url + "  %s %o", req.payload.id, req.query);
    let userId = req.payload.id;
    let userTrackId = req.query.id;
    let similarity = req.query.similarity
      ? parseFloat(req.query.similarity)
      : 0.85;
    if (typeof similarity != "number" || similarity < 0 || similarity > 1) {
      return res
        .status(400)
        .send("Paramter similarity muss als Zahl zwischen 0 und 1 vorliegen ");
    }
    let userTrack = await Tracks.findById(userTrackId);
    if (userTrack == null) {
      return res.status(404).send("Benutzerstrecke existiert nicht");
    }

    let user = await Users.findById(userId, "-salt -hash");

    // Die Strecken der Freunde muss länger als die Benutzerstrecke sein,
    // sonst wird diese beim Vergleich abgeschnitten und das würde beim
    // Ermitteln der Zeiten zu Problemen führen, weil compareTracks immer auf
    // die Länge der kürzeren Streck schneidet
    let trackOfFriends = await Tracks.find({
      user: { $in: user.friendList },
      totalDistance: { $gte: userTrack.totalDistance }
    })
      .populate("user", "username")
      .populate({
        path: "activities",
        options: {
          sort: { finishTime: -1 }, // Ermittle die Aktivität mit der schnellsten Zeit
          limit: 1
        }
      });

    let compares = await Promise.all(
      trackOfFriends.map(async track => {
        return compareTracks(userTrack, track, false, true);
      })
    );
    // debug("compares", compares);
    result = {};
    compares.forEach(compare => {
      debug("compare %o", compare);
      if (compare.similarity >= similarity) {
        // Prüfe, ob bereits ein Eintrag exisitiert, wenn nicht fülle das Feld
        result[compare.track2.user.username] = result[
          compare.track2.user.username
        ] || {
          similarity: compare.similarity,
          track: compare.track2,
          finishTime: compare.track2.finishTime
        };

        // Prüfe, ob die Ähnlichkeit besser ist und ersetze den Eintrag
        result[compare.track2.user.username] =
          result[compare.track2.user.username].similarity <= compare.similarity
            ? {
                similarity: compare.similarity,
                track: compare.track2,
                finishTime: compare.track2.finishTime
              }
            : result[compare.track2.user.username];
      }
    });
    return res.status(200).send(result);
  } catch (error) {
    debug(error);
    res.status(500).end();
  }
});

module.exports = router;
