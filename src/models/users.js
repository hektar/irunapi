const mongoose = require("mongoose");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const debug = require("debug")("irun:usersModel");

const { Schema } = mongoose;

const UsersSchema = new Schema({
  username: String,
  hash: String,
  salt: String,
  friendList: [{ type: Schema.Types.ObjectId, ref: "Users" }],
  sentFriendRequests: [{ type: Schema.Types.ObjectId, ref: "Users" }],
  friendRequests: [{ type: Schema.Types.ObjectId, ref: "Users" }],
  exp: { type: Number, default: 0 },
});

/**
 * Do not declare statics using ES6 arrow functions (=>).
 * Arrow functions explicitly prevent binding this, so the above examples will not work because of the value of this.
 */

UsersSchema.methods.setPassword = function (password) {
  debug("User setPassword");
  this.salt = crypto.randomBytes(16).toString("hex");
  this.hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
};

UsersSchema.methods.validatePassword = function (password) {
  debug("User validatePassword");
  const hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
  return this.hash === hash;
};

UsersSchema.methods.generateJWT = function () {
  debug("User generateJWT");

  return jwt.sign(
    {
      username: this.username,
      id: this._id
    },
    "secret"
    // { expiresIn: '1d' } // Ein Tag
  );
};

UsersSchema.methods.toAuthJSON = function () {
  debug("User toAuthJSON");
  return {
    _id: this._id,
    username: this.username,
    exp: this.exp,
    token: this.generateJWT()
  };
};

mongoose.model("Users", UsersSchema);
