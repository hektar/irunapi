const mongoose = require("mongoose");
const debug = require("debug")("irun:tracksModel");

const { Schema } = mongoose;

const TracksSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "Users", required: true },
  title: String,
  maxElevation: Number,
  minElevation: Number,
  date: Date,
  elevationDifference: Number,
  ascent: Number,
  descent: Number,
  totalDistance: Number,
  activities: [{ type: Schema.Types.ObjectId, ref: "Activities" }],
  coords: {
    type: [
      {
        longitude: Number,
        latitude: Number,
        speed: Number,
        elevation: Number,
        timestamp: Number,
        time: Number,
        isCheckpoint: { type: Boolean, default: false },
        distanceToPrevPoint: Number,
        distanceFromStart: Number,
        sortsequence: Number
      }
    ],
    required: true
  },
  trackProfile: {
    type: [
      {
        longitude: Number,
        latitude: Number,
        time: Number,
        elevation: Number,
        distanceFromStart: Number
      }
    ],
    required: true
  },
  originTrack: { type: Schema.Types.ObjectId, ref: "Tracks", default: null }
});

mongoose.model("Tracks", TracksSchema);
