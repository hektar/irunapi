const mongoose = require("mongoose");
const debug = require("debug")("irun:tracksModel");

const { Schema } = mongoose;

const ActivitiesSchema = new Schema({
  title: String,
  date: Date,
  user: { type: Schema.Types.ObjectId, ref: "Users", required: true },
  track: { type: Schema.Types.ObjectId, ref: "Tracks", required: true },
  finishTime: Number,
  checkpointTimes: [
    {
      checkpointId: Schema.Types.ObjectId,
      title: String,
      distance: Number,
      time: Number
    }
  ]
});

mongoose.model("Activities", ActivitiesSchema);
