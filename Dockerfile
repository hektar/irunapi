FROM node:10
WORKDIR /usr/src/irunapi/
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8000