const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const session = require("express-session");
const cors = require("cors");
const mongoose = require("mongoose");
const errorHandler = require("errorhandler");
const debug = require("debug")("irun:server");
const MongoStore = require("connect-mongo")(session);

// Konfiguriere isProduction Variable
const isProduction = process.env.NODE_ENV === "production";

// Init App
const app = express();

//Baue eine Verbindung zu DB und konfiguriere Mongoose
mongoose
  .connect("mongodb://mongo:27017/irun", {
    useNewUrlParser: true,
    user: "iRunAPI",
    pass: "_bplan4711_"
  })
  .then(() => debug("DB connected"))
  .catch(err => debug("DB connection error"));

//mongoose.set("debug", !isProduction);
mongoose.set("useFindAndModify", false);

// Konfiguriere Express
app.use(cors());
app.use(bodyParser.urlencoded({ limit: "50mb", extended: false }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(express.static(path.join(__dirname, "public")));
app.use(
  session({
    secret: "da39a3ee5e6b4b0d3255bfef95601890afd80709",
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: false
  })
);

if (!isProduction) {
  app.use(errorHandler());
}

// Lade Models & Routen
require("./src/models/users");
require("./src/models/tracks");
require("./src/models/activities");
require("./src/config/passport");
app.use(require("./src/routes"));

app.listen(8000, () => debug("Server running on http://localhost:8000/"));
