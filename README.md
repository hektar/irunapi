# iRunApi
iRunAPI (c) 2019-2020 Tolga Kocer <tolga.kocer.tk@gmail.com>

## Voraussetzung
Es muss Docker (Docker-Desktop) auf dem System installiert sein. Ist Docker installiert, muss vorher die OpenElevationAPI gestartet werden. Dafür folgt man den Anweisungen auf:

https://github.com/Jorl17/open-elevation/blob/master/docs/host-your-own.md

Hinweis: Als Datenset wurde nur Deutschland genommen. Im Ordner open-elevation/data (auf der CD) befindet sich der Vollständigkeit halber die Datei srtm_germany_dtm.tif. Diese Datei wurde genutzt, um die einzelnen Tiles zu erstellen. Bevor der Server gestartet wird, muss diese Datei vom Ordner data entfernt werden, damit der Server die Tiles verwendet!

## Installation

Über die Kommandozeile bzw. das Terminal im Projekt-Wurzelverzeichnis kann mit dem folgenden Befehl die iRunAPI gestartet werden:

`docker-compose up -d --build`

Die iRunApi läuft im irunapi_default Network. Jetzt kann die OpenElevationAPI gestartet werden. Dafür wechselt man in den entsprechenden Projekt-Ordner. Wichtig ist, dass beim Starten als Netzwerk die irunapi_default gesetzt wird.

`docker run --network="irunapi_default" -d -t -i -v $(pwd)/data:/code/data -p 8080:8080 openelevation/open-elevation`

Mit `docker ps` werden alle laufenden Container angezeigt. Unter der Spalte NAMES steht der von Docker zugewiesene Name für die OpenElevationAPI. Dieser wird für den folgenden Befehl benötigt.

`docker network inspect irunapi_default`

Dort steht die IP-Adresse von der OpenElevationAPI. Hier sucht man nach dem Eintrag, welches aus NAMES entnommen wurde (im Bsp. sleepy_lehmann). Die Ausagbe sieht in etwa so aus: 

``` javascript
 "Containers": {
            "d7da7a454ba30836d33812b0227fbb4f236bba683342246abcc1e90baba2d0d2": {
                "Name": "sleepy_lehmann",
                "EndpointID": "189541cc8459c77eca4d290d907e11cd9c4c4141d5c8888ea8fb7698dfe534d4",
                "MacAddress": "02:42:ac:19:00:02",
                "IPv4Address": "172.25.0.2/16",
                "IPv6Address": ""
            }
        },

```

Die IP-Adresse der OpenElevationAPI muss im Quellcode der iRunAPI angepasst werden. Dafür öffnet man die Datei routes/api/tracks/index.js und setzt die IP-Adresse in Zeile 11 ein. Achtung, der Port 8080 darf nicht gelöscht werden!

```javascript
const elevationAPI_IP = "http://172.25.0.2:8080/"; 
```

Nun müsste die iRunAPI ordnungsgemäß laufen. Man kann nochmal `docker ps` ausführen und sich vergewissern. Hinweis: Die IP-Adresse der iRunAPI muss auch im Quellcode von iRun eingefügt werden (siehe README.md im iRun-Ordner)



